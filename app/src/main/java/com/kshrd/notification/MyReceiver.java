package com.kshrd.notification;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.support.v7.app.NotificationCompat;

public class MyReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        Intent i = new Intent(context, DetailActivity.class);
        i.putExtra("ID", 1);
        i.putExtra("TITLE", "Sport News");

        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0,
                i, PendingIntent.FLAG_UPDATE_CURRENT);


        NotificationCompat.Builder builder = new NotificationCompat.Builder(
                context);

        Uri uri = Uri.parse("android.resource://" + context.getPackageName() + "/" + R.raw.jinglebell);
        long[] pattern = {500, 500, 500};

        builder
                .setContentTitle("Title")
                .setContentText("Content Text")
                .setLargeIcon(BitmapFactory.decodeResource(context.getResources(),
                        R.drawable.ic_audiotrack_black_24dp))
                .setSmallIcon(R.drawable.ic_attach_money_black_24dp)
                .setTicker("Ticker")
                .setAutoCancel(true)
                .setPriority(NotificationCompat.PRIORITY_MAX)
                .setSound(uri)
                .setLights(Color.BLUE, 500, 500)
                .setVibrate(pattern)
                .addAction(R.drawable.ic_audiotrack_black_24dp, "Music", pendingIntent)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        int notificationID = (int) (System.currentTimeMillis() / 1000);
        notificationManager.notify(notificationID, builder.build());

    }
}

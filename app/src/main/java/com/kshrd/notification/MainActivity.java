package com.kshrd.notification;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.renderscript.RenderScript;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.NotificationCompat;
import android.view.View;
import android.widget.RemoteViews;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.btnSimple).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(MainActivity.this, DetailActivity.class);
                i.putExtra("ID", 1);
                i.putExtra("TITLE", "Sport News");

                PendingIntent pendingIntent = PendingIntent.getActivity(MainActivity.this, 0,
                        i, PendingIntent.FLAG_UPDATE_CURRENT);


                NotificationCompat.Builder builder = new NotificationCompat.Builder(
                        MainActivity.this);

                // Default Sound
                //Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

                Uri uri = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.jinglebell);
                long[] pattern = {500, 500, 500};

                builder
                        .setContentTitle("Title")
                        .setContentText("Content Text")
                        .setLargeIcon(BitmapFactory.decodeResource(getResources(),
                                R.drawable.ic_audiotrack_black_24dp))
                        .setSmallIcon(R.drawable.ic_attach_money_black_24dp)
                        .setTicker("Ticker")
                        .setAutoCancel(true)
                        .setPriority(NotificationCompat.PRIORITY_MAX)
                        .setSound(uri)
                        .setLights(Color.BLUE, 500, 500)
                        .setVibrate(pattern)
                        .addAction(R.drawable.ic_audiotrack_black_24dp, "Music", pendingIntent)
                        .setContentIntent(pendingIntent);

                NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                int notificationID = (int) (System.currentTimeMillis() / 1000);
                notificationManager.notify(notificationID, builder.build());

            }
        });

        findViewById(R.id.btnInboxStyle).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();
                String[] events = new String[3];
                events[0] = new String("1) Message for implicit intent");
                events[1] = new String("2) big view Notification");
                events[2] = new String("3) from HRD!");
                // Sets a title for the Inbox style big view
                inboxStyle.setBigContentTitle("More Details:");
                // Moves events into the big view
                for (int i = 0; i < events.length; i++) {
                    inboxStyle.addLine(events[i]);
                }

                NotificationCompat.Builder builder = new NotificationCompat.Builder(
                        MainActivity.this);

                builder
                        .setContentTitle("Title")
                        .setContentText("Content Text")
                        .setSmallIcon(R.drawable.ic_attach_money_black_24dp)
                        .setTicker("Ticker")
                        .setStyle(inboxStyle);

                NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                int notificationID = (int) (System.currentTimeMillis() / 1000);
                notificationManager.notify(notificationID, builder.build());


            }
        });

        findViewById(R.id.btnBigPicture).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                NotificationCompat.BigPictureStyle bigPictureStyle = new android.support.v4.app.NotificationCompat.BigPictureStyle();
                bigPictureStyle.bigPicture(BitmapFactory.decodeResource(getResources(), R.drawable.mountain));
                bigPictureStyle.bigLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.ic_audiotrack_black_24dp));
                bigPictureStyle.setBigContentTitle("New Promotion");

                NotificationCompat.Builder builder = new NotificationCompat.Builder(
                        MainActivity.this);

                builder
                        .setContentTitle("Title")
                        .setContentText("Content Text")
                        .setSmallIcon(R.drawable.ic_attach_money_black_24dp)
                        .setTicker("Ticker")
                        .setStyle(bigPictureStyle);

                NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                int notificationID = (int) (System.currentTimeMillis() / 1000);
                notificationManager.notify(notificationID, builder.build());
            }
        });

        findViewById(R.id.btnBigText).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                NotificationCompat.BigTextStyle bigTextStyle = new android.support.v4.app.NotificationCompat.BigTextStyle();
                bigTextStyle.bigText("Big Text");
                bigTextStyle.setBigContentTitle("Context Title");
                bigTextStyle.setSummaryText("Summary Text");

                NotificationCompat.Builder builder = new NotificationCompat.Builder(
                        MainActivity.this);

                builder
                        .setContentTitle("Title")
                        .setContentText("Content Text")
                        .setSmallIcon(R.drawable.ic_attach_money_black_24dp)
                        .setTicker("Ticker")
                        .setStyle(bigTextStyle);

                NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                int notificationID = (int) (System.currentTimeMillis() / 1000);
                notificationManager.notify(notificationID, builder.build());
            }
        });

        findViewById(R.id.btnCustom).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                RemoteViews remoteViews = new RemoteViews(getPackageName(), R.layout.custom_notification);
                remoteViews.setTextViewText(R.id.tvText, "Hello World!");

                NotificationCompat.Builder builder = new NotificationCompat.Builder(
                        MainActivity.this);

                builder
                        .setContentTitle("Title")
                        .setContentText("Content Text")
                        .setSmallIcon(R.drawable.ic_attach_money_black_24dp)
                        .setTicker("Ticker")
                        .setContent(remoteViews);

                NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                int notificationID = (int) (System.currentTimeMillis() / 1000);
                notificationManager.notify(notificationID, builder.build());
            }
        });


    }
}

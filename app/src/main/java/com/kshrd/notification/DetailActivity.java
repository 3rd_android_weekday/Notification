package com.kshrd.notification;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

public class DetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            int id = bundle.getInt("ID");
            String title = bundle.getString("TITLE");
            Toast.makeText(this, "ID: " + id + ", TITLE: " + title, Toast.LENGTH_SHORT).show();
        }
    }
}
